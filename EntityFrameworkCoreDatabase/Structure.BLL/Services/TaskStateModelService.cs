﻿//using AutoMapper;
//using Structure.Common.DTO;
//using Structure.DAL.Entities;
//using Structure.DAL.Interfaces;
//using Structure.DAL.Services;
//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace Structure.BLL.Services
//{
//    public class TaskStateModelService : IService<TaskStateDTO>
//    {
//        private readonly IUnitOfWork _unitOfWork;
//        private readonly IMapper _mapper;
//        public TaskStateModelService(IUnitOfWork unitOfWork, IMapper mapper)
//        {
//            _unitOfWork = unitOfWork;
//            _mapper = mapper;
//        }
//        public void Create(TaskStateDTO item)
//        {
//            _unitOfWork.TaskStateModels.Create(_mapper.Map<TaskState>(item));
//        }
//        public bool Delete(int id)
//        {
//            return _unitOfWork.TaskStateModels.Delete(id);
//        }
//        public TaskStateDTO Get(int id)
//        {
//            var result = _unitOfWork.TaskStateModels.Get(id);
//            return _mapper.Map<TaskStateDTO>(result);
//        }
//        public IEnumerable<TaskStateDTO> GetAll()
//        {
//            var result = _unitOfWork.TaskStateModels.GetAll();
//            return _mapper.Map<IEnumerable<TaskStateDTO>>(result);
//        }
//        public bool Update(TaskStateDTO item)
//        {
//            return _unitOfWork.TaskStateModels.Update(_mapper.Map<TaskState>(item));
//        }
//    }
//}
