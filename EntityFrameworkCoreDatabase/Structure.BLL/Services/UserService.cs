﻿using AutoMapper;
using Structure.Common.DTO;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using Structure.DAL.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.BLL.Services
{
    public class UserService : IService<UserDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public void Create(UserDTO item)
        {
            _unitOfWork.Users.Create(_mapper.Map<User>(item));
            _unitOfWork.SaveChanges();
            return;
        }
        public bool Delete(int id)
        {
            _unitOfWork.Users.Delete(id);
            _unitOfWork.SaveChanges();
            return true;
        }
        public UserDTO Get(int id)
        {
            var result = _unitOfWork.Users.Get(id);
            return _mapper.Map<UserDTO>(result);
        }
        public IEnumerable<UserDTO> GetAll()
        {
            var result = _unitOfWork.Users.GetAll();
            return _mapper.Map<IEnumerable<UserDTO>>(result);
        }
        public bool Update(UserDTO item)
        {
            _unitOfWork.Users.Update(_mapper.Map<User>(item));
            _unitOfWork.SaveChanges();
            return true;
        }
    }
}
