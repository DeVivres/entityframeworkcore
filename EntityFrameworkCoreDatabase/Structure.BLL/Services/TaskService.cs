﻿using AutoMapper;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using Structure.Common.DTO;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using Structure.DAL.Services;
using System;
using System.Collections.Generic;


namespace Structure.BLL.Services
{
    public class TaskService : IService<TaskDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public void Create(TaskDTO item)
        {
            _unitOfWork.Tasks.Create(_mapper.Map<Task>(item));
            _unitOfWork.SaveChanges();
            return;
        }
        public bool Delete(int id)
        {
            _unitOfWork.Tasks.Delete(id);
            _unitOfWork.SaveChanges();
            return true;
        }
        public TaskDTO Get(int id)
        {
            var result =_unitOfWork.Tasks.Get(id);
            return _mapper.Map<TaskDTO>(result);
        }
        public IEnumerable<TaskDTO> GetAll()
        {
            var result = _unitOfWork.Tasks.GetAll();
            return _mapper.Map<IEnumerable<TaskDTO>>(result);
        }
        public bool Update(TaskDTO item)
        {
            _unitOfWork.Tasks.Update(_mapper.Map<Task>(item));
            _unitOfWork.SaveChanges();
            return true;
        }
    }
}
