﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Structure.Common.DTO;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using Structure.DAL.Services;

namespace Structure.BLL.Services
{
    public class ProjectService : IService<ProjectDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public void Create(ProjectDTO item)
        {
            _unitOfWork.Projects.Create(_mapper.Map<Project>(item));
            _unitOfWork.SaveChanges();
            return;
        }
        public bool Delete(int id)
        {
            _unitOfWork.Projects.Delete(id);
            _unitOfWork.SaveChanges();
            return true;
        }
        public ProjectDTO Get(int id)
        {
            var result = _unitOfWork.Projects.Get(id);
            return _mapper.Map<ProjectDTO>(result);
        }
        public IEnumerable<ProjectDTO> GetAll()
        {
            var result = _unitOfWork.Projects.GetAll();
            return _mapper.Map<IEnumerable<ProjectDTO>>(result);
        }
        public bool Update(ProjectDTO item)
        {
            _unitOfWork.Projects.Update(_mapper.Map<Project>(item));
            _unitOfWork.SaveChanges();
            return true;
        }
    }
}
