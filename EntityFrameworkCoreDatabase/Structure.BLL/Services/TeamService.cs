﻿using AutoMapper;
using Structure.Common.DTO;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using Structure.DAL.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.BLL.Services
{
    public class TeamService : IService<TeamDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TeamService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public void Create(TeamDTO item)
        {
            _unitOfWork.Teams.Create(_mapper.Map<Team>(item));
            _unitOfWork.SaveChanges();
            return;
        }
        public bool Delete(int id)
        {
            _unitOfWork.Teams.Delete(id);
            _unitOfWork.SaveChanges();
            return true;
        }
        public TeamDTO Get(int id)
        {
            var result = _unitOfWork.Teams.Get(id);
            return _mapper.Map<TeamDTO>(result);
        }
        public IEnumerable<TeamDTO> GetAll()
        {
            var result = _unitOfWork.Teams.GetAll();
            return _mapper.Map<IEnumerable<TeamDTO>>(result);
        }
        public bool Update(TeamDTO item)
        {
            _unitOfWork.Teams.Update(_mapper.Map<Team>(item));
            _unitOfWork.SaveChanges();
            return true;
        }
    }
}
