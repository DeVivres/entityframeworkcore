﻿using AutoMapper;
using Structure.Common.DTO;
using Structure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>().ReverseMap();
        }
    }
}
