﻿using AutoMapper;
using Structure.Common.DTO;
using Structure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>().ReverseMap();
        }
    }
}
