﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.DAL.Entities
{
        public enum TaskStateModel
        {
            Created,
            Started,
            Finished,
            Canceled
        }
}
