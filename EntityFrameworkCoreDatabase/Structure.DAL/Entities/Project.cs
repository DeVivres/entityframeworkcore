﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.DAL.Entities
{
    public sealed class Project
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }
        [JsonProperty("authorId")]
        public int AuthorId { get; set; }
        [JsonProperty("teamId")]
        public int TeamId { get; set; }
        public override string ToString()
        {
            return string.Format($"Project Id: {Id}\n" +
                                 $"Name: {Name}\n" +
                                 $"Description: {Description}\n" +
                                 $"Created At: {CreatedAt}\n" +
                                 $"Deadline: {Deadline}\n" +
                                 $"Author Id: {AuthorId}\n" +
                                 $"Team Id: {TeamId}\n");
        }
    }
}
