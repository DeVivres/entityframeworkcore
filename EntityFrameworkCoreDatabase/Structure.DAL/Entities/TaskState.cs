﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.DAL.Entities
{
    public class TaskState
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
