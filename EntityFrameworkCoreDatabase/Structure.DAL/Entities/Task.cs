﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.DAL.Entities
{
    public sealed class Task
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("finishedAt")]
        public DateTime FinishedAt { get; set; }
        [JsonProperty("state")]
        public TaskState State { get; set; }
        [JsonProperty("projectId")]
        public int ProjectId { get; set; }
        [JsonProperty("performerId")]
        public int PerformerId { get; set; }
        public override string ToString()
        {
            return string.Format($"Project Id: {Id}\n" +
                                 $"Name: {Name}\n" +
                                 $"Description: {Description}\n" +
                                 $"Created At: {CreatedAt}\n" +
                                 $"Finished At: {FinishedAt}\n" +
                                 $"State: {State}\n" +
                                 $"Project Id: {ProjectId}\n" +
                                 $"Performer Id: {PerformerId}\n");
        }
    }
}
