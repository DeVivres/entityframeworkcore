﻿using Structure.DAL.Entities;
using Structure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ProjectRepository Projects { get; }
        TaskRepository Tasks { get; }
        //TaskStateRepository TaskStateModels { get; }
        TeamRepository Teams { get; }
        UserRepository Users { get; }
        int SaveChanges();
    }
}
