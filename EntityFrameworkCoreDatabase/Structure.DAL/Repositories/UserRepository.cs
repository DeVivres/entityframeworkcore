﻿using Microsoft.EntityFrameworkCore;
using Structure.DAL.Context;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Structure.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly DatabaseContext _databaseContext;
        public UserRepository(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public void Create(User item)
        {
            _databaseContext.Users.Add(item);
        }

        public bool Delete(int id)
        {
            var item = _databaseContext.Users.FirstOrDefault(a => a.Id == id);

            if(item == null)
            {
                return false;
            }
            _databaseContext.Users.Remove(item);
            return true;
        }

        public User Get(int id)
        {
            var item = _databaseContext.Users.FirstOrDefault(a => a.Id == id);
            return item;
        }

        public IEnumerable<User> GetAll()
        {
            return _databaseContext.Users;
        }

        public bool Update(User item)
        {
            var result = _databaseContext.Users.FirstOrDefault(a => a.Id == item.Id);

            if (result == null)
            {
                throw new ArgumentException("No user with such Id");
            }

            _databaseContext.Entry(item).State = EntityState.Modified;

            return true;
        }
    }
}
