﻿//using Structure.DAL.Context;
//using Structure.DAL.Entities;
//using Structure.DAL.Interfaces;
//using System;
//using System.Collections.Generic;
//using System.Linq;

//namespace Structure.DAL.Repositories
//{
//    public class TaskStateRepository : IRepository<TaskState>
//    {
//        private readonly DatabaseContext _databaseContext;
//        public TaskStateRepository(DatabaseContext databaseContext)
//        {
//            _databaseContext = databaseContext;
//        }
//        private int CreateId()
//        {
//            return _databaseContext.TaskStates.Max(s => s.Id) + 1;
//        }
//        public void Create(TaskState item)
//        {
//            item.Id = CreateId();
//            _databaseContext.TaskStates.Add(item);
//        }

//        public bool Delete(int id)
//        {
//            var item = _databaseContext.TaskStates.FirstOrDefault(a => a.Id == id);

//            if(item == null)
//            {
//                return false;
//            }
//            _databaseContext.TaskStates.Remove(item);
//            return true;
//        }

//        public TaskState Get(int id)
//        {
//            var item = _databaseContext.TaskStates.FirstOrDefault(a => a.Id == id);
//            return item;
//        }

//        public IEnumerable<TaskState> GetAll()
//        {
//            return _databaseContext.TaskStates;
//        }

//        public bool Update(TaskState item)
//        {
//            var result = _databaseContext.TaskStates.FirstOrDefault(a => a.Id == item.Id);

//            if (result == null)
//            {
//                throw new ArgumentException("No state with such Id");
//            }

//            _databaseContext.Entry(item).State = EntityState.Modified;

//            return true;
//        }
//    }
//}
