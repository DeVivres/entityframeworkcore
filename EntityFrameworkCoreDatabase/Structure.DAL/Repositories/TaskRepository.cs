﻿using Microsoft.EntityFrameworkCore;
using Structure.DAL.Context;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Structure.DAL.Repositories
{
    public class TaskRepository : IRepository<Task>
    {
        private readonly DatabaseContext _databaseContext;
        public TaskRepository(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }
        public void Create(Task item)
        {
            _databaseContext.Tasks.Add(item);
        }

        public bool Delete(int id)
        {
            var item = _databaseContext.Tasks.FirstOrDefault(a => a.Id == id);

            if(item == null)
            {
                return false;
            }

            _databaseContext.Tasks.Remove(item);
            return true;
        }

        public Task Get(int id)
        {
            var item = _databaseContext.Tasks.FirstOrDefault(a => a.Id == id);
            return item;
        }

        public IEnumerable<Task> GetAll()
        {
            return _databaseContext.Tasks;
        }

        public bool Update(Task item)
        {
            var result = _databaseContext.Tasks.FirstOrDefault(a => a.Id == item.Id);

            if (result == null)
            {
                throw new ArgumentException("No task with such Id");
            }

            _databaseContext.Entry(item).State = EntityState.Modified;

            return true;
        }
    }
}
