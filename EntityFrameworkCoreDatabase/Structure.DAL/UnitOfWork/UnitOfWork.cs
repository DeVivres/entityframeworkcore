﻿using Structure.DAL.Context;
using Structure.DAL.Interfaces;
using Structure.DAL.Repositories;
using System;

namespace Structure.DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DatabaseContext _databaseContext;
        private bool _disposedDatabase;
        public UnitOfWork(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
            _disposedDatabase = false;
        }
        private ProjectRepository _projectRepository;
        private TaskRepository _taskRepository;
        //private TaskStateRepository _taskStateModelRepository;
        private TeamRepository _teamRepository;
        private UserRepository _userRepository;
        public ProjectRepository Projects
        {
            get
            {
                if(_projectRepository == null)
                {
                    _projectRepository = new ProjectRepository(_databaseContext);
                }
                return _projectRepository;
            }
        }
        public TaskRepository Tasks
        {
            get
            {
                if (_taskRepository == null)
                {
                    _taskRepository = new TaskRepository(_databaseContext);
                }
                return _taskRepository;
            }
        }
        //public TaskStateRepository TaskStateModels
        //{
        //    get
        //    {
        //        if (_taskStateModelRepository == null)
        //        {
        //            _taskStateModelRepository = new TaskStateRepository(_databaseContext);
        //        }
        //        return _taskStateModelRepository;
        //    }
        //}
        public TeamRepository Teams
        {
            get
            {
                if (_teamRepository == null)
                {
                    _teamRepository = new TeamRepository(_databaseContext);
                }
                return _teamRepository;
            }
        }
        public UserRepository Users
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository(_databaseContext);
                }
                return _userRepository;
            }
        }

        public int SaveChanges()
        {
            return _databaseContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public virtual void Dispose(bool _disposingDatabase)
        {
            if (!_disposedDatabase)
            {
                if (_disposingDatabase)
                {
                    _databaseContext.Dispose();
                }
                _disposedDatabase = true;
            }
        }
    }
}
