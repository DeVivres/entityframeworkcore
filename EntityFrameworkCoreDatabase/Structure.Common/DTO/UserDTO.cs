﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Structure.Common.DTO
{
    public sealed class UserDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
    }
}
