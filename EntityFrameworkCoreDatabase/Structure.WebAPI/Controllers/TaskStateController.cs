﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Mvc;
//using Structure.BLL.Services;
//using Structure.Common.DTO;

//namespace Structure.WebAPI.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class TaskStateModelController : ControllerBase
//    {
//        private readonly TaskStateModelService _taskStateService;

//        public TaskStateModelController(TaskStateModelService taskstateService)
//        {
//            _taskStateService = taskstateService;
//        }

//        [HttpGet("TaskStates")]
//        public IActionResult GetAllTaskStates()
//        {
//            var result = _taskStateService.GetAll();
//            return Ok(result);
//        }
//        [HttpGet("{id}")]
//        public IActionResult Get(int id)
//        {
//            var result = _taskStateService.Get(id);
//            return Ok(result);
//        }
//        [HttpPost]
//        public IActionResult Create([FromBody] TaskStateDTO value)
//        {
//            _taskStateService.Create(value);
//            return Ok();
//        }

//        [HttpPut]
//        public IActionResult Update([FromBody] TaskStateDTO value)
//        {
//            _taskStateService.Update(value);
//            return Ok(value);
//        }
//        [HttpDelete("{id}")]
//        public IActionResult Delete(int id)
//        {
//            var deleted = _taskStateService.Delete(id);
//            return NoContent();
//        }
//    }
//}
