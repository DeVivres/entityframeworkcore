﻿
using Microsoft.AspNetCore.Mvc;
using Structure.Common.DTO;
using Structure.DAL.Services;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IService<ProjectDTO> _projectService;

        public ProjectController(IService<ProjectDTO> projecsService)
        {
            _projectService = projecsService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var projects = _projectService.GetAll();
            return Ok(projects);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var project = _projectService.Get(id);
            return Ok(project);
        }

        [HttpPost]
        public IActionResult Create([FromBody] ProjectDTO project)
        {
            _projectService.Create(project);
            return Ok();
        }

        [HttpPut]
        public IActionResult Update([FromBody] ProjectDTO project)
        {
            _projectService.Update(project);
            return Ok(project);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _projectService.Delete(id);
            return NoContent();
        }
    }
}
