﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Structure.BLL.Services;
using Structure.Common.DTO;
using Structure.DAL.Services;

namespace Structure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly IService<TaskDTO> _taskService;

        public TaskController(IService<TaskDTO> taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _taskService.GetAll();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result =_taskService.Get(id);
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Create([FromBody] TaskDTO value)
        {
            _taskService.Create(value);
            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult Update([FromBody] TaskDTO value)
        {
            _taskService.Update(value);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _taskService.Delete(id);
            return NoContent();
        }
    }
}
