﻿using System.Collections.Generic;
using Structure.DAL.Entities;

namespace Structure.ConsoleApplication.Services
{
    class DataCollection
    {
        private readonly HttpService _httpService;

        public ICollection<Project> Projects;
        public ICollection<Task> Tasks;
        public ICollection<Team> Teams;
        public ICollection<User> Users;

        public DataCollection()
        {
            _httpService = new HttpService();

            Projects = _httpService.GetAllProjects().Result;
            Tasks = _httpService.GetAllTasks().Result;
            Teams = _httpService.GetAllTeams().Result;
            Users = _httpService.GetAllUsers().Result;
        }
    }
}
