﻿using Newtonsoft.Json;
using Structure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Structure.ConsoleApplication.Services
{
    class HttpService
    {
        HttpClient client = new HttpClient();
        public HttpService()
        {
            client.BaseAddress = new Uri("http://localhost:51546/api/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<ICollection<Project>> GetAllProjects()
        {
            var answer = await client.GetStringAsync("Project");
            return JsonConvert.DeserializeObject<ICollection<Project>>(answer);
        }

        public async Task<Project> GetProjectById(int id)
        {
            var answer = await client.GetStringAsync($"Project/{id}");
            return JsonConvert.DeserializeObject<Project>(answer);   
        }

        public async Task<ICollection<DAL.Entities.Task>> GetAllTasks()
        {
            var answer = await client.GetStringAsync("Task");
            return JsonConvert.DeserializeObject<ICollection<DAL.Entities.Task>>(answer);
        }

        public async Task<DAL.Entities.Task> GetTaskById(int id)
        {
            var answer = await client.GetStringAsync($"Task/{id}");
            return JsonConvert.DeserializeObject<DAL.Entities.Task>(answer);
        }

        public async Task<ICollection<TaskState>> GetTasksStates()
        {
            var answer = await client.GetStringAsync("TaskStates");
            return JsonConvert.DeserializeObject<ICollection<TaskState>>(answer);
        }

        public async Task<ICollection<Team>> GetAllTeams()
        {
            var answer = await client.GetStringAsync("Team");
            return JsonConvert.DeserializeObject<ICollection<Team>>(answer);
        }

        public async Task<Team> GetTeamById(int id)
        {
            var answer = await client.GetStringAsync($"Team/{id}");
            return JsonConvert.DeserializeObject<Team>(answer);
        }

        public async Task<ICollection<User>> GetAllUsers()
        {
            var answer = await client.GetStringAsync("User");
            return JsonConvert.DeserializeObject<ICollection<User>>(answer);
        }

        public async Task<User> GetUserById(int id)
        {
            var answer = await client.GetStringAsync($"User/{id}");
            return JsonConvert.DeserializeObject<User>(answer);
        }
    }
}
